#     Copyright (C) 2023  Gary Thompson <coding@garythompson.au>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module contains files for random generation of height maps, typically by using
noise generators.

Useful references:

See:  https://github.com/tizilogic/PyFastNoiseLite/blob/master/src/pyfastnoiselite
/pyfastnoiselite.pyx
And:  https://auburn.github.io/FastNoiseLite/

"""
from pyfastnoiselite import pyfastnoiselite as fnl
import numpy as np


def generate_perlin_terrain(
    width=100,
    length=100,
    scale=5,
    frequency=0.01,
    seed=int(np.random.randint(-(2**16), 2**16)),
):
    # noinspection DuplicatedCode
    noise = fnl.FastNoiseLite(seed=seed)  # Random seed
    noise.noise_type = fnl.NoiseType.NoiseType_Perlin
    noise.frequency = frequency

    # Generate a 2D height_map
    height_map = np.zeros((width, length))
    for x in range(width):
        for y in range(length):
            height_map[x][y] = (1 + noise.get_noise(x, y)) / 2 * scale

    return height_map


def generate_cellular_terrain(
    width=100,
    length=100,
    scale=5,
    frequency=0.01,
    seed=int(np.random.randint(-(2**16), 2**16)),
    distance_function=fnl.CellularDistanceFunction.CellularDistanceFunction_Euclidean,
    cellular_return_type=fnl.CellularReturnType.CellularReturnType_Distance,
    jitter=1.2,
):
    noise = fnl.FastNoiseLite(seed=seed)  # Random seed
    noise.noise_type = fnl.NoiseType.NoiseType_Cellular
    noise.frequency = frequency
    noise.cellular_distance_function = distance_function
    noise.cellular_return_type = cellular_return_type
    # noinspection DuplicatedCode
    noise.cellular_jitter = jitter

    # Generate a 2D height_map
    height_map = np.zeros((width, length))
    for x in range(width):
        for y in range(length):
            height_map[x][y] = (1 + noise.get_noise(x, y)) / 2 * scale

    return height_map
