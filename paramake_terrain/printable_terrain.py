import dataclasses
import logging
import pathlib
from functools import cached_property
from typing import Optional, Literal, NoReturn

import cadquery
import numpy as np

from paramake_terrain.height_map import (
    increase_contrast,
    apply_gradient_border,
    invert_height_map,
)
from paramake_terrain.image import height_map_as_image
from paramake_terrain.noise import generate_cellular_terrain, generate_perlin_terrain
from paramake_terrain.solid import solid_from_height_map
from tests.snapshot_tests.utils import export_step

logger = logging.getLogger(__name__)


@dataclasses.dataclass
class PrintableTerrain:
    """
    This class acts as a convenient interface for capturing parameters for forming terrain
    from noise.  The init parameters will not be read again once the terrain is generated.

    It has been created with the needs of the paramake_gridfinity initially in mind and will expand
    with appropriate use cases.
    """

    # Noise
    width_mm: int
    height_mm: int
    dots_per_mm: int
    frequency: float
    seed: int
    invert: bool

    # Mesh Pre-processing
    increase_contrast: bool
    contrast_clip_low: Optional[float]
    contrast_clip_high: Optional[float]
    gradient_border_width: Optional[int]

    # Mesh
    bottom_padding: int | float
    noise_scale_mm: int | float
    mesh_height_resolution: None | int | float
    step_height: None | int | float
    compress: None | Literal["delta"]
    extrude: Literal["extrude", "loft", "loft-ruled"]
    limit: None | int
    compression_setting: float

    # Migrating to kwargs for new features

    # Noise Params
    noise_type: Literal["cellular", "perlin"] = "cellular"
    cellular_jitter: int | float | None = 1.2

    def __str__(self):
        return "-".join(
            [
                str(self.width_mm),
                str(self.height_mm),
                str(self.dots_per_mm),
                self.noise_type,
                str(self.frequency),
                str(self.seed),
                str(self.invert),
                str(self.increase_contrast),
                str(self.contrast_clip_low),
                str(self.contrast_clip_high),
                str(self.gradient_border_width),
                str(self.bottom_padding),
                str(self.noise_scale_mm),
                str(self.mesh_height_resolution),
                str(self.step_height),
                str(self.compress),
                str(self.extrude),
                str(self.limit),
                str(self.compression_setting),
            ]
        )

    def copy(self, width_mm=None, height_mm=None) -> "PrintableTerrain":
        """
        A convenience function for making a copy with new height and width parameters
        """
        new_terrain = PrintableTerrain(**self.__dict__)
        new_terrain.width_mm = width_mm
        new_terrain.height_mm = height_mm
        return new_terrain

    @cached_property
    def raw_height_map(self) -> np.ndarray:
        """
        This property represents the original height map of the terrain.  It is not intended
        to change once generated.
        """
        hm_resolution_width = int(self.width_mm * self.dots_per_mm)
        hm_resolution_length = int(self.height_mm * self.dots_per_mm)

        if self.noise_type == "cellular":
            hm = generate_cellular_terrain(
                width=hm_resolution_width,
                length=hm_resolution_length,
                frequency=self.frequency,
                scale=1,
                seed=self.seed,
                jitter=self.cellular_jitter or 1.2
            )
        elif self.noise_type == "perlin":
            hm = generate_perlin_terrain(
                width=hm_resolution_width,
                length=hm_resolution_length,
                frequency=self.frequency,
                scale=1,
                seed=self.seed,
            )
        else:
            raise NotImplemented(self.noise_type)

        logger.debug("raw_height_map produced with range %f -> %f", hm.min(), hm.max())
        return hm

    @cached_property
    def processed_height_map(self) -> np.ndarray:
        hm = self.raw_height_map

        if self.increase_contrast:
            hm = increase_contrast(
                hm,
                clip_high=self.contrast_clip_high,
                clip_low=self.contrast_clip_low,
            )
        if self.gradient_border_width:
            hm = apply_gradient_border(hm, self.gradient_border_width)
        if self.invert:
            hm = invert_height_map(hm)

        return hm

    @staticmethod
    def enable_np_array_debug() -> NoReturn:
        """
        A convenience method for outputting full array data
        """
        # noinspection PyTypeChecker
        np.set_printoptions(
            precision=2, suppress=True, threshold=np.inf, linewidth=np.inf
        )

    def export_png(self, filename: pathlib.Path) -> NoReturn:
        image = height_map_as_image(self.processed_height_map)
        image.save(filename.with_suffix(".png"))

    @cached_property
    def solid(self) -> cadquery.Workplane:
        solid = solid_from_height_map(
            height_map=self.processed_height_map,
            bottom_padding=self.bottom_padding,  # + noise_scale_mm if invert else 0,
            mesh_width_mm=self.width_mm,
            height_mm=self.noise_scale_mm,
            mesh_height_resolution=self.mesh_height_resolution,
            step_height=self.step_height,
            compress=self.compress,
            extrude=self.extrude,
            limit=self.limit,
            compression_setting=self.compression_setting,
        )
        return solid

    def export_step(self, filename: pathlib.Path) -> NoReturn:
        export_step(filename.with_suffix(".step"), self.solid)
