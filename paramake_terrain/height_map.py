#     Copyright (C) 2023  Gary Thompson <coding@garythompson.au>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Functions that specialise in height map pre-processing and post-processing
"""
from typing import Optional

import numpy as np


def map_height_map_to_vectors(
    height_map: np.ndarray,
    mesh_scale: int | float = 1.0,
    height_mm: int | float = 2.0,
    mesh_height_resolution: None | int | float = 0.2,
    height_bias_mm: int | float = 0.0,
) -> np.ndarray:
    """
    Takes a height map as a 2D array of values 0 to 1 and convert it into
    a list of vertices.

    :return: A 2-dimensional array of tuples representing each point in the mesh, corresponding
    to height_map.
    """
    width, length = height_map.shape

    # Create vertices for the top surface
    surface_vertices = np.zeros((width, length, 3))
    for x in range(width):
        for y in range(length):
            if mesh_height_resolution is not None:
                z = (
                    round(height_map[x][y] * height_mm / mesh_height_resolution)
                    * mesh_height_resolution
                    + height_bias_mm
                )
            else:
                z = height_map[x][y] * height_mm + height_bias_mm
            surface_vertices[x][y] = [
                x * mesh_scale,
                y * mesh_scale,
                z,
            ]
    return surface_vertices


def invert_height_map(
    height_map: np.ndarray,
) -> np.ndarray:
    """
    Assumes a height map of range 0 to 1
    """
    min_val = height_map.min()
    max_val = height_map.max()
    assert min_val >= 0
    assert max_val <= 1
    return 1 - height_map


def increase_contrast(
    height_map: np.ndarray,
    clip_low: Optional[float] = None,
    clip_high: Optional[float] = None,
) -> np.ndarray:
    """
    This function is a height_map processing function that increases the
    contrast of the data.  By default, it keeps all values between 0 (black) and 1 (white).

    :param height_map: Input map
    :param clip_low: When provided, will clip the low value to this value and re-normalise
    :param clip_high: When provided, will clip the high end to this value and re-normalise
    """
    # First normalise from 0 to 1
    min_val = height_map.min()
    max_val = height_map.max()
    range_val = max_val - min_val
    height_map = (height_map - min_val) / range_val

    # Clip the resulting data
    if clip_low or clip_high:
        height_map = np.clip(height_map, clip_low or 0, clip_high or 1)
        min_val = height_map.min()
        max_val = height_map.max()
        range_val = max_val - min_val
        height_map = (height_map - min_val) / range_val

    return height_map


# noinspection SpellCheckingInspection
def apply_gradient_border(image, border_width):
    """
    Apply a gradient border to a 2D image represented as a numpy ndarray.

    :param image: 2D numpy ndarray representing the image.
    :param border_width: Width of the border.
    :return: Image with gradient border applied.
    """
    border_mask = np.zeros_like(image)

    # Create gradient for each edge
    for i in range(border_width):
        gradient_value = (i + 1) / border_width
        border_mask[(border_width - 1) - i, :] = gradient_value  # Top border
        border_mask[-border_width + i, :] = gradient_value  # Bottom border
        border_mask[:, (border_width - 1) - i] = (
            border_mask[:, (border_width - 1) - i] + gradient_value
        )  # Left border
        border_mask[:, -border_width + i] = (
            border_mask[:, -border_width + i] + gradient_value
        )  # Right border

    border_mask = np.clip(border_mask, 0, 1)

    # Invert the mask to blend with the image
    inverted_mask = invert_height_map(border_mask)

    # np.set_printoptions(precision=2, suppress=True, threshold=np.inf, linewidth=np.inf)
    # print(border_mask)

    # Blend the original image with the border
    blended_image = (
        image * inverted_mask + border_mask * 1
    )  # 1 is white border, 0 is black border

    return blended_image
