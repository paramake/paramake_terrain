#     Copyright (C) 2023  Gary Thompson <coding@garythompson.au>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module provides an interface to stl mesh format.  STL meshes are mostly
handy for debugging or importing into CAD software but are not that useful for
handling programmatically.
"""
import numpy as np
# noinspection PyPackageRequirements
from stl import mesh

from paramake_terrain.height_map import map_height_map_to_vectors
from paramake_terrain.vertices import flatten_vertices


def height_map_to_stl(
    height_map,
    filename,
    bottom_padding=1,
    mesh_width_mm=60,
    height_mm: float = 2.0,
    mesh_height_resolution=0.2,
):
    width, length = height_map.shape
    mesh_scale = mesh_width_mm / width

    top_vertices = map_height_map_to_vectors(
        height_map, mesh_scale, height_mm, mesh_height_resolution, bottom_padding
    )
    top_vertices = flatten_vertices(top_vertices)

    # Create faces for the top surface
    top_faces = []
    for x in range(width - 1):
        for y in range(length - 1):
            v1 = x * length + y
            v2 = (x + 1) * length + y
            v3 = (x + 1) * length + y + 1
            v4 = x * length + y + 1
            top_faces.append([v1, v2, v3])
            top_faces.append([v1, v3, v4])

    # Create vertices for the bottom surface (at z=-10 for debugging)
    bottom_vertices = np.zeros((width * length, 3))
    for x in range(width):
        for y in range(length):
            bottom_vertices[x * length + y] = [
                x * mesh_scale,
                y * mesh_scale,
                0,
            ]

    # Concatenate top and bottom vertices
    vertices = np.concatenate((top_vertices, bottom_vertices))

    # Offset for bottom vertices
    vertex_offset = len(top_vertices)

    # Create faces for the bottom surface
    bottom_faces = []
    for x in range(width - 1):
        for y in range(length - 1):
            v1 = x * length + y + vertex_offset
            v2 = (x + 1) * length + y + vertex_offset
            v3 = (x + 1) * length + y + 1 + vertex_offset
            v4 = x * length + y + 1 + vertex_offset
            bottom_faces.append([v1, v3, v2])
            bottom_faces.append([v1, v4, v3])

    # Create side faces
    side_faces = []

    # Top edge
    for y in range(length - 1):
        v_top_1 = y
        v_top_2 = y + 1
        v_bottom_1 = v_top_1 + vertex_offset
        v_bottom_2 = v_top_2 + vertex_offset
        side_faces.append([v_top_1, v_bottom_1, v_bottom_2])
        side_faces.append([v_top_1, v_bottom_2, v_top_2])

        # Bottom edge
        for y2 in range(length - 1):
            v_top_1 = (width - 1) * length + y2
            v_top_2 = (width - 1) * length + y2 + 1
            v_bottom_1 = v_top_1 + vertex_offset
            v_bottom_2 = v_top_2 + vertex_offset
            side_faces.append([v_top_1, v_top_2, v_bottom_2])
            side_faces.append([v_top_1, v_bottom_2, v_bottom_1])

        # Left edge
        for x in range(width - 1):
            v_top_1 = x * length
            v_top_2 = (x + 1) * length
            v_bottom_1 = v_top_1 + vertex_offset
            v_bottom_2 = v_top_2 + vertex_offset
            side_faces.append([v_top_1, v_top_2, v_bottom_2])
            side_faces.append([v_top_1, v_bottom_2, v_bottom_1])

        # Right edge
        for x in range(width - 1):
            v_top_1 = x * length + (length - 1)
            v_top_2 = (x + 1) * length + (length - 1)
            v_bottom_1 = v_top_1 + vertex_offset
            v_bottom_2 = v_top_2 + vertex_offset
            side_faces.append([v_top_1, v_bottom_1, v_bottom_2])
            side_faces.append([v_top_1, v_bottom_2, v_top_2])

    # Concatenate all faces
    faces = np.concatenate((top_faces, bottom_faces, side_faces))

    # Create the mesh
    terrain_mesh = mesh.Mesh(np.zeros(faces.shape[0], dtype=mesh.Mesh.dtype))
    for i, f in enumerate(faces):
        for j in range(3):
            terrain_mesh.vectors[i][j] = vertices[f[j], :]

    # Write the mesh to an STL file
    terrain_mesh.save(filename)
