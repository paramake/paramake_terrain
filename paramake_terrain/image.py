#     Copyright (C) 2023  Gary Thompson <coding@garythompson.au>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module contains image conversion functions such as generating a height map
from a greyscale image.
"""
import pathlib

from PIL import Image
import numpy as np


def create_height_map_from_png(
    png_path: pathlib.Path, min_height: int | float, max_height: int | float
) -> np.ndarray:
    # Open the PNG file
    with Image.open(png_path) as img:
        # Convert the image to grayscale
        grayscale_img = img.convert("L")

        # Convert the grayscale image to a numpy array
        grayscale_array = np.array(grayscale_img)

        # Normalize the values to be between 0 and 1
        normalized_array = grayscale_array / 255.0

        # Scale the normalized array to get the heightmap
        heightmap = normalized_array * (max_height - min_height) + min_height

        return heightmap


def height_map_as_image(height_map: np.ndarray) -> Image:
    """
    Converts a 2D ndarray height map into a PNG image.
    Values of 1 in the height map correspond to white in the image, and 0 to black.

    :param height_map: 2D ndarray with values from 0 to 1
    """
    # Ensure height_map values are within [0, 1] range
    height_map = np.clip(height_map, 0, 1)

    # Convert height map to an image (values from 0 to 255)
    image_data = (height_map * 255).astype(np.uint8)

    # Create an image from the array (in 'L' mode for grayscale)
    image = Image.fromarray(image_data, mode='L')

    return image


def save_height_map_as_png(height_map: np.ndarray, file_path: pathlib.Path | str) -> None:
    """
    Calls height_map_as_image and then saves the file.
    """
    image = height_map_as_image(height_map)

    # Save the image
    image.save(str(file_path))
