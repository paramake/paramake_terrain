#     Copyright (C) 2023  Gary Thompson <coding@garythompson.au>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
This module provides functions for interfacing with solid objects.
"""
import logging
from typing import Literal

import cadquery
import numpy as np

from paramake_terrain.height_map import map_height_map_to_vectors
from paramake_terrain.vertices import stepped_line, delta_compress

logger = logging.getLogger(__name__)


def solid_from_height_map(
    height_map: np.ndarray,
    bottom_padding: int | float = 1,
    mesh_width_mm: int | float = 60,
    height_mm: int | float = 2.0,
    mesh_height_resolution: None | int | float = None,
    step_height: None | int | float = 0.5,
    compress: None | Literal["delta"] = None,
    extrude: Literal["extrude", "loft", "loft-ruled"] = "extrude",
    limit=None,
    compression_setting=0.001,
) -> cadquery.Workplane:
    """
    Unfortunately, there's no standard converter to STEP for an STL file.
    STEP does not handle mesh information well, this function will try and produce an
    acceptable mesh.
    """
    width, length = height_map.shape
    mesh_scale = mesh_width_mm / width

    surface_vertices = map_height_map_to_vectors(
        height_map,
        mesh_scale,
        height_mm,
        mesh_height_resolution=mesh_height_resolution,
        height_bias_mm=bottom_padding,
    )

    # Collect polyline vertices
    sections = []
    for raw_cross_section in surface_vertices:
        # Upper surface vertices
        if step_height:
            cross_section = list(
                stepped_line(raw_cross_section, 1, 2, step_height=step_height)
            )
            logger.debug(
                f"Step change to polyline %i -> %i",
                len(raw_cross_section),
                len(cross_section),
            )
        else:
            cross_section = [(i[1], i[2]) for i in raw_cross_section]
        sections.append(cross_section)

    # TODO:  When lofting a compressed profile, try and determine a set of coordinates to include
    # if compress == "delta" and extrude.startswith("loft"): # Lofting works best with a
    # consistent grid.  Use Delta compression to determine x points on the grid for x in range(
    # width): x_pos = surface_vertices[x][0][0] y_start = surface_vertices[x][0][1] y_end =
    # surface_vertices[x][-1][1]
    #
    #         # Upper surface vertices
    #         if stepped:
    #             v = stepped_line(surface_vertices[x], 1, 2)
    #         else:
    #             v = [(i[1], i[2]) for i in surface_vertices[x]]
    #
    #         v = list(delta_compress(v))

    wp = cadquery.Workplane("YZ")
    solid = wp
    last_x_pos = 0
    c = 0
    for x_index, cross_section in enumerate(sections):
        x_pos = surface_vertices[x_index][0][0]
        y_start = cross_section[0][0]  # Cross-section is on YZ Plane
        y_end = cross_section[-1][0]

        match compress:
            case "delta" if extrude.startswith("loft"):
                pass
            case "delta":
                cross_section = delta_compress(cross_section, diff=compression_setting)
            case _:
                pass

        # Complete the bottom level of the polyline
        cross_section = [(y_start, 0)] + list(cross_section) + [(y_end, 0)]

        # Sides and base
        if extrude.startswith("loft"):
            # noinspection PyTypeChecker
            solid = (
                solid.workplane(offset=x_pos - last_x_pos)
                .polyline(cross_section)
                .close()
            )
        elif x_index == (width - 1):
            continue  # for extrusion, skip the final slice
        else:  # Extrusion
            next_x_pos = surface_vertices[x_index + 1][0][0]
            extrude_distance = next_x_pos - x_pos
            # noinspection PyTypeChecker
            polyline = wp.workplane(offset=x_pos).polyline(cross_section).close()
            # extrusion = polyline.extrude(extrude_distance)
            extrusion = polyline.extrude(extrude_distance)
            solid = solid.union(extrusion)
        last_x_pos = x_pos
        c += 1
        if c % 20 == 0:
            logger.debug(f"Loop {c} of {width}")
        if limit and c > limit:
            break

    if extrude.startswith("loft"):
        # Ruled produces a more stable output, but a larger one too
        # Without compression, lofting time is quite fast
        logger.debug("Starting loft")
        solid = solid.loft(ruled=extrude.endswith("ruled"), combine=True)
        logger.debug("Loft Complete")

    return solid
