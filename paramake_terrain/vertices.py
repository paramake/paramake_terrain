#     Copyright (C) 2023  Gary Thompson <coding@garythompson.au>
#
#     This program is free software: you can redistribute it and/or modify
#     it under the terms of the GNU General Public License as published by
#     the Free Software Foundation, either version 3 of the License, or
#     (at your option) any later version.
#
#     This program is distributed in the hope that it will be useful,
#     but WITHOUT ANY WARRANTY; without even the implied warranty of
#     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#     GNU General Public License for more details.
#
#     You should have received a copy of the GNU General Public License
#     along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Functions that assist with processing polylines or vertex arrays
"""
import numpy as np


def flatten_vertices(vertices_3d: np.ndarray) -> np.ndarray:
    """
    Takes a 3D array of vertices and flattens it into a 2D array.
    Each row in the 2D array represents a vertex with [x, y, z] coordinates.
    """
    width, length, _ = vertices_3d.shape
    return vertices_3d.reshape((width * length, 3))


def stepped_line(
    vertices, x_index=0, y_index=1, step_height=0
) -> list[tuple[float, float]]:
    """
    Add additional vertices to a polyline to prevent interpolation along the x-axis.
    """
    last_v = None
    for raw_v in vertices:
        x = raw_v[x_index]
        y = raw_v[y_index]
        v = (x, y)
        # noinspection PyUnresolvedReferences
        last_y = last_v[1] if step_height and last_v is not None else y
        if abs(last_y - y) >= step_height:
            # interim_x = (x - last_x) * 0.5 + last_x
            interim_x = x
            # noinspection PyUnresolvedReferences
            yield interim_x, last_y
        yield v
        last_v = v


def delta_compress(
    vertices: list[tuple[float, float]], diff=0.0001
) -> list[tuple[float, float]]:
    """
    Compresses the polyline by removing vertices that do not change
    by diff amount in the y direction.
    """
    last_yield = None
    last_v = None
    c = 0
    t = 0
    for v in vertices:
        t += 1

        if last_yield and last_v:
            yield_delta_y = abs(v[1] - last_yield[1])
            step_delta_x = abs(v[0] - last_v[0])
        else:
            # Force the next lines to progress with artificial values
            yield_delta_y = 2 * diff
            step_delta_x = 1

        # If there is a duplicate x value, that means there is a detail to preserve
        if step_delta_x == 0:
            yield last_v
            yield v
            c += 2
            last_yield = v
        elif yield_delta_y < diff:
            pass
        else:
            yield v
            c += 1
            last_yield = v
        last_v = v

    if last_yield != last_v:
        yield last_v
        c += 1
    print(f"delta_compress {t} -> {c}")


# def compress_line2(vertices, x_index, y_index):
#     """
#     This was an option explored previously that may be considered again
#     """
#     from scipy.spatial.distance import Euclidean
#
#     compressed = [vertices[0]]
#     for v in vertices[1:]:
#         if euclidean(compressed[-1], v) > 0.1:
#             compressed.append(v)
#     return compressed
