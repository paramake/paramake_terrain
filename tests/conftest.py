"""
Common setup and fixtures for this project.
"""
import numpy as np
import pytest

import tests.paths
from paramake_terrain.image import create_height_map_from_png


@pytest.fixture(scope="session")
def gary_png_height_map() -> np.ndarray:
    yield create_height_map_from_png(
        tests.paths.FIXTURES_DIR / "gary.png",
        1,
        0,
    )
