import pytest

from paramake_terrain.stl import height_map_to_stl
from tests.snapshot_tests.utils import assert_snapshot_unchanged


@pytest.mark.parametrize(
    "bottom_padding,mesh_width_mm,height_mm,mesh_height_resolution", [(1, 60, 2.0, 0.2)]
)
def test_height_map_to_stl__detailed_mesh_from_image(
    gary_png_height_map,
    bottom_padding,
    mesh_width_mm,
    height_mm,
    mesh_height_resolution,
):
    snapshot_file_name = (
        f"height_map_to_stl__detailed_mesh_from_image"
        f"-{bottom_padding}"
        f"-{mesh_width_mm}"
        f"-{height_mm}"
        f"-{mesh_height_resolution}"
        f".stl"
    )

    with assert_snapshot_unchanged(snapshot_file_name, skip_header=56) as file_path:
        height_map_to_stl(
            height_map=gary_png_height_map,
            filename=file_path,
            bottom_padding=bottom_padding,
            mesh_width_mm=mesh_width_mm,
            height_mm=height_mm,
            mesh_height_resolution=mesh_height_resolution,
        )

    # Final manual verification step - User needs to comment this out after manual inspection
    # assert (
    #     False
    # ), "Manual verification pending. Comment out this line after verification."
