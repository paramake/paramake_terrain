from typing import Literal

import pytest

from paramake_terrain.image import save_height_map_as_png
from paramake_terrain.noise import generate_perlin_terrain, generate_cellular_terrain
from paramake_terrain.solid import solid_from_height_map
from tests.snapshot_tests.utils import assert_snapshot_unchanged, export_step
import tests.paths


@pytest.mark.parametrize(
    (
        "bottom_padding,mesh_width_mm,height_mm,mesh_height_resolution,"
        "step_height,compress,extrude,limit,compression_setting,"
        "inspected"
    ),
    [
        (1, 60, 2.0, None, 0.5, None, "extrude", 35, 0.001, True),
        # Still contains some minor artefacts, but will do for now
        (1, 50, 2.5, None, 0.2, "delta", "extrude", 35, 0.2, True),
    ],
)
def test_solid_from_height_map__detailed_solid_from_image(
    gary_png_height_map,
    bottom_padding,
    mesh_width_mm,
    height_mm,
    mesh_height_resolution,
    step_height,
    compress,
    extrude: Literal["extrude", "loft", "loft-ruled"],
    limit,
    compression_setting,
    inspected
):
    snapshot_file_name = (
        f"solid_from_height_map__detailed_solid_from_image"
        f"-{bottom_padding}"
        f"-{mesh_width_mm}"
        f"-{height_mm}"
        f"-{mesh_height_resolution}"
        f"-{step_height}"
        f"-{compress}"
        f"-{extrude}"
        f"-{limit}"
        f"-{compression_setting}"
        f".step"
    )

    with assert_snapshot_unchanged(snapshot_file_name, skip_header=800) as file_path:
        solid = solid_from_height_map(
            height_map=gary_png_height_map,
            bottom_padding=bottom_padding,
            mesh_width_mm=mesh_width_mm,
            height_mm=height_mm,
            mesh_height_resolution=mesh_height_resolution,
            step_height=step_height,
            compress=compress,
            extrude=extrude,
            limit=limit,
            compression_setting=compression_setting,
        )
        export_step(file_path, solid)

    # Final manual verification step - User needs to comment this out after manual inspection
    assert (
        inspected
    ), "Manual verification pending. Comment out this line after verification."
