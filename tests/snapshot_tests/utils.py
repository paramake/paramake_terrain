"""
Utilities used across all the snapshot tests
"""
import pathlib
import tempfile
from typing import Generator

import cadquery
import pytest
import shutil
from contextlib import contextmanager

import tests.paths


def export_step(filepath: pathlib.Path, stack: cadquery.Workplane):

    stack.val().exportStep(str(filepath))


def compare_files(file1: pathlib.Path, file2: pathlib.Path, header_len: int = 0):
    """
    Compare two files, ignoring the first few header bytes as
    these contain date or time stamps in STL and STEP files
    """
    with open(file1, "rb") as f1, open(file2, "rb") as f2:
        f1.read(header_len)
        f2.read(header_len)

        return f1.read() == f2.read()


@contextmanager
def assert_snapshot_unchanged(
    snapshot_file_name: str, skip_header=0
) -> Generator[pathlib.Path, None, None]:
    snapshot_path = tests.paths.SNAPSHOTS_DIR / snapshot_file_name

    with tempfile.NamedTemporaryFile(delete=False) as fp:
        fp.close()  # The file is not needed here
        fp_path = pathlib.Path(fp.name)
        yield fp_path

        if not snapshot_path.exists():
            # If snapshot doesn't exist, create it and fail the test for manual verification
            shutil.copy(fp_path, snapshot_path)
            pytest.fail(
                f"Snapshot file {snapshot_path} created. Needs manual verification."
            )
        else:
            assert compare_files(
                fp_path, snapshot_path, header_len=skip_header
            ), "Snapshot comparison failed"
