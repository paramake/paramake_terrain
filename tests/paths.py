import pathlib

BASE_TESTS_DIR = pathlib.Path(__file__).parent
FIXTURES_DIR = BASE_TESTS_DIR / "fixtures"
SNAPSHOTS_DIR = BASE_TESTS_DIR / "snapshot_tests" / "snapshots"
