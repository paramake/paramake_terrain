======
README
======

Paramake
========

This library is one part of the parametric making tools.  While parametric CAD is available using OpenSCad, FreeCAD
and commercial offerings like OnShape, there are some projects that will benefit from the extensive Python libraries.

Most of these tools are based on CadQuery.

The set of Paramake tools are available here:  https://gitlab.com/paramake


PrintableTerrain
=======

The Paramake terrain library focuses on using terrain generation.  The intent is not for creating 3D terrains, but
terrain-like objects that can add texture or interesting detail to 3D Models.


Source Code Management
======================

The main repository for the code is on GitLab:  https://gitlab.com/paramake/paramake_terrain.

The stable branch is the production branch.  However, this won't be used much until releases are being released on to
PyPi.

Most development is on the dev branch using a simple dev branch model.


Tool Chain
==========

Paramake uses `Python Poetry <https://python-poetry.org/>`_ for dependency management.  This library is currently
not available on `PyPi <https://pypi.org/>`_ but can be added to your project using python poetry as follows::

    # Add paramake_terrain using poetry
    poetry add -D paramake_terrain@https://gitlab.com/paramake/paramake_terrain.git#dev


Usage
=====

For the moment, check the stl and solid test files for examples of generation.  There is also the printable_terrain
module that provides an overarching interface with examples in the trials directory.

Testing
=======

Due to the complexity of testing CAD outputs, current testing approaches will focus on manual inspection and snapshots.

As the snapshots are comparing raw CAD output, these will break when the dependencies change version.  At this point,
they will need to be reinspected.


Acknowledgements
================

In no particular order.

* https://choosealicense.com/
* Python Software Foundation
* Python Poetry
* CadQuery
* The 3D print communities and making communities
* ChatGPT
