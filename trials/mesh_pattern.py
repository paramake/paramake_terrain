"""
This trial is to find appropriate parameters for achieving various mesh patterns
"""
import pathlib

from paramake_terrain.printable_terrain import PrintableTerrain

BASE_PATH = pathlib.Path(__file__).parent
OUTPUT_PATH = BASE_PATH / "output"

if not OUTPUT_PATH.exists():
    OUTPUT_PATH.mkdir()

# Note:  Gridfinity Bin calculations
# Width and depth are gu x 42mm - 2 x wall thickness
# Depth is gu x 7mm -4.75 - floor thickness


# This was the first print trial as a material removal mask for a gridfinity container
first_wall_negative = PrintableTerrain(
    width_mm=13,
    height_mm=33,
    dots_per_mm=2,
    frequency=0.088,
    seed=1373,
    invert=True,
    increase_contrast=False,
    contrast_clip_low=None,
    contrast_clip_high=None,
    gradient_border_width=None,
    bottom_padding=0,
    noise_scale_mm=6.0,
    mesh_height_resolution=None,
    step_height=1,
    compress=None,
    extrude="loft",
    limit=None,
    compression_setting=0.001,
)

# This did not originally generate as it was too large for lofting
# The version below works okay
larger_wall_negative = PrintableTerrain(
    width_mm=68,
    height_mm=152,
    dots_per_mm=2,
    frequency=0.085,
    seed=1373,
    invert=False,
    increase_contrast=False,
    contrast_clip_low=None,
    contrast_clip_high=None,
    gradient_border_width=None,
    bottom_padding=0,
    noise_scale_mm=6.0,
    mesh_height_resolution=None,
    step_height=1,
    compress=None,
    extrude="loft",
    limit=None,
    compression_setting=0.001,
)


# These settings seem to reliably produce a larger mesh
larger_wall_negative2 = PrintableTerrain(
    width_mm=68,
    height_mm=152,
    dots_per_mm=1,
    frequency=0.060,
    seed=1373,
    invert=True,
    increase_contrast=True,
    contrast_clip_low=None,
    contrast_clip_high=0.7,
    gradient_border_width=8,
    bottom_padding=1,
    noise_scale_mm=5.0,
    mesh_height_resolution=None,
    step_height=None,
    compress=None,
    extrude="loft",
    limit=None,
    compression_setting=0.001,
)


webbing_1 = PrintableTerrain(
    width_mm=68,
    height_mm=152,
    dots_per_mm=1,
    frequency=0.051,
    seed=419165,
    invert=True,
    increase_contrast=True,
    contrast_clip_low=None,
    contrast_clip_high=0.6,
    gradient_border_width=8,
    bottom_padding=1,
    noise_scale_mm=6.0,
    mesh_height_resolution=None,
    step_height=None,
    compress=None,
    extrude="loft",
    limit=None,
    compression_setting=0.001,
)

# With new features, perlin noise might be worth investigating
webbing_2 = PrintableTerrain(
    width_mm=68,
    height_mm=152,
    dots_per_mm=1,
    noise_type="perlin",
    frequency=0.045,
    seed=419182,
    increase_contrast=True,
    contrast_clip_low=0.1,
    contrast_clip_high=0.9,
    invert=True,
    gradient_border_width=8,
    bottom_padding=1,
    noise_scale_mm=3.0,
    mesh_height_resolution=None,
    step_height=None,
    compress=None,
    extrude="loft",
    limit=None,
    compression_setting=0.001,
)


def trial_optimal_loft_for_webbing(output_solid=False):
    """
    This function looks at tuning heightmaps at the PNG phase.  Some mesh goals include:

    * Increasing contrast
    * Adding a border for a smoother edge

    """

    for terrain in [
        first_wall_negative,
        larger_wall_negative,
        larger_wall_negative2,
        webbing_1,
        webbing_2
    ]:
        snapshot_file_name = (
            f"trial_optimal_loft_for_webbing" f"-{str(terrain)}" f".png"
        )

        image_path = OUTPUT_PATH / snapshot_file_name
        terrain.export_png(image_path)

        if output_solid:
            print("Generating Solid")
            step_path = OUTPUT_PATH / f"{snapshot_file_name}.step"
            terrain.export_step(step_path)


if __name__ == "__main__":
    trial_optimal_loft_for_webbing(output_solid=False)
